"""
- Primeira questao: Criar classe Time

- a) retornar o media de goals por partida
- b) calcular a média de pontos por partida
- c) calcular o saldo de gols do time
- d) atualizar de acordo com os resultados de uma partida (vitória/derrota/empate,
    gols feitos e gols sofridos)
- e) retornar os seguintes status do time: partidas jogadas, pontos feitos, saldo de gols,
    média de gols por partida, média de pontos por partida
"""

from ..utils.get_attribute import get_attribute


class Team(object):
    def __init__(self, name, optional_parameters={}):
        self.name = name
        self.points = get_attribute(optional_parameters, 'points', 0)
        self.goals_pro = get_attribute(optional_parameters, 'goals', 0)
        self.own_goal = get_attribute(optional_parameters, 'own_goal', 0)
        self.matches = get_attribute(optional_parameters, 'matches', 0)

    @property
    def goals_average(self):
        """

        @parameters:
            - none

        @returns
            - int - returns a avareage of goals made by the team
        """
        if self.matches == 0 or self.goals_pro == 0:
            return 0
        return self.goals_pro / self.matches

    @property
    def total_goals_average(self):
        """

        @parameters:
            - none

        @returns
            - int -  goals average
        """
        if self.matches == 0 or self.goals == 0:
            return 0
        return self.goals / self.matches

    @property
    def points_average(self):
        """
        @parameters:
            - none

        @returns
            - int -  points average
        """
        if self.matches == 0 or self.points == 0:
            return 0
        return self.points / self.matches

    @property
    def goals(self):
        """
        @parameters:
            - none

        @returns:
            - int
        """
        return self.goals_pro - self.own_goal

    def update_team_deprecated(self, result, goals_pro=0, goals_loss=0):
        """
        Updates the team attributes
        @parameters:
            - result:
                - one of the following chars:
                    => 'v' victory
                    => 'l' loss
                    => 't' tie
            - goals_pro:
                - int
            - goals_loss:
                - int

        @returns:
            - none
        """

        # we don't need this "result". We could use goals_pro - own_goals to infer the result.
        # using because of the requirements
        if result == "v":
            self.points += 3
        elif result == 'l':
            pass
        elif result == 't':
            self.points += 1
        else:
            raise Exception("wrong result char. Please use 'v', 'l', or 't' ")

        self.goals_pro += goals_pro
        self.own_goal += goals_loss
        self.matches += 1

    def update_team(self, result=None, goals_pro=0, goals_loss=0):
        """
        Updates the team attributes infering result
        @parameters:
            - result:
                - one of the following chars:
                    => 'v' victory
                    => 'l' loss
                    => 't' tie
            - goals_pro:
                - int
            - goals_loss:
                - int

        @returns:
            - none
        """

        # infering the result by score
        if result is None:
            if goals_pro == goals_loss:
                self.points += 1
            if goals_pro > goals_loss:
                self.points += 3
            if goals_pro < goals_loss:
                pass
        else:
            if result == "v":
                self.points += 3
            elif result == 'l':
                pass
            elif result == 't':
                self.points += 1
            else:
                raise Exception("wrong result char. Please use 'v', 'l', or 't' ")



        self.goals_pro += goals_pro
        self.own_goal += goals_loss
        self.matches += 1

    def __str__(self):
        """
        @Parameters:
            - none

        @returns:
            - str:
                String with Team info
                    - total of matches
                    - points
                    - total goals
                    - goals average
                    - points average
        """
        return (
            "Time: {}\n"
            "============\n"
            "- total de partidas: \t\t{}\n"
            "- pontos: \t\t\t{}\n"
            "- total gols: \t\t\t{}\n"
            "- media de gols feitos: \t{}\n"
            "- media saldo gols: \t\t{}\n"
            "- media de pontos: \t\t{}\n"
        ).format(
            self.name, self.matches, self.points, self.goals,
            self.goals_average, self.total_goals_average, self.points_average
        )

def main():
    """
    main method

    void => void
    """


    team = Team("TimeA")
    print(team)
