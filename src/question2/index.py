"""
Q2)
    considerar as tabelas e criar queries especificas
"""

from .db import DBConnetion

def main():
    """
    """
    db = DBConnetion.get_cursor()


    def question_a():
        """
        > all purchases

        @Parameters:
            - none

        @returns:
            - none
        """
        db.execute("""
            SELECT
                usuarios.id as usuario_id,
                usuarios.nome,

                compras.id as compras_id,
                compras.valor as valor_compra,
                compras.item as item_compra

            FROM compras

            INNER JOIN usuarios ON compras.id_usuario = usuarios.id

            ORDER BY usuarios.id ASC
        """)

        for line in db.fetchall():
            print(line)

    def question_b():
        """
        All user and their purchases
        @Parameters:
            - none

        @returns:
            - none
        """
        db.execute("""
            SELECT
                usuarios.id as usuario_id,
                usuarios.nome,

                compras.id as compras_id,
                compras.valor as valor_compra,
                compras.item as item_compra

            FROM usuarios

            LEFT JOIN compras ON compras.id_usuario = usuarios.id
            GROUP BY compras.id
            ORDER BY usuarios.id ASC
        """)

        for line in db.fetchall():
            print(line)

    print("========= questao A")
    question_a()


    print("\n\n========= questao B")
    question_b()


    DBConnetion.close_connection()
