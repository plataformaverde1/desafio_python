"""
this module creates and handle the connection with the database.

we are using sqlite only because it is the simplest way

We are using static attributes to reuse the same object in different calls
"""

import sqlite3
import os


class DBConnetion(object):
    __conn = None

    def __init__(self):
        pass

    @staticmethod
    def get_connection():
        """
        Opens a connetcion with sqlite (keep it simple)

        - using singleton

        @Parameters:
            - none

        @returns:
            - sqlite db
        """

        if DBConnetion.__conn is not None:
            return DBConnetion.__conn

        DBConnetion.__conn = sqlite3.connect(
            os.path.dirname(__file__) + os.path.sep + "sqlite.db"
        )
        return DBConnetion.__conn

    @staticmethod
    def close_connection():
        """
        docstring
        """
        if DBConnetion.__conn is not None:
            DBConnetion.__conn.close()

        DBConnetion.__conn = None

    @staticmethod
    def get_cursor():
        """
        docstring
        """
        return DBConnetion.get_connection().cursor()
