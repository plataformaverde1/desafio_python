def get_attribute(obj, key, default):
    return obj[key] if hasattr(obj, key) else default
