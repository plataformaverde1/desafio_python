from .db import DBConnetion


class UserController(object):
    """
    defines basic methods to handle database
    """
    table_name = 'users'

    @staticmethod
    def store(user_name, user_id=None):
        """ Saves one instance in DB """

        curr = DBConnetion.get_cursor()
        connection = DBConnetion.get_connection()

        # autoincrement id
        sql = ("INSERT INTO {} (name) "
               "VALUES ( \"{}\" );").format(UserController.table_name, user_name)

        # manually set id
        if user_id is not None:
            sql = ("INSERT INTO {} (id, name) "
                   "VALUES ( \"{}\", \"{}\" );"
                   ).format(UserController.table_name, user_id, user_name)

        curr.execute(sql)
        connection.commit()

        return {"new_user_id": curr.lastrowid}

    @staticmethod
    def index(user_id=None):
        """ Finds elements in this collection """

        curr = DBConnetion.get_cursor()

        sql = "SELECT * FROM {}".format(UserController.table_name)

        if user_id is not None:
            sql = ("SELECT id, name FROM {} WHERE id = \"{}\""
                   ).format(UserController.table_name, user_id)

        curr.execute(sql)
        users = []
        for row in curr.fetchall():
            users.append({'id': row[0], 'name': row[1]})

        return {'users': users}

    @staticmethod
    def delete(user_id):
        """Removes on element by id"""
        curr = DBConnetion.get_cursor()
        connection = DBConnetion.get_connection()

        sql = (
            "DELETE from {} where id = {}"
        ).format(UserController.table_name, user_id)

        curr.execute(sql)
        connection.commit()

        return {"users_affected": curr.rowcount}

    @staticmethod
    def update(user_id, new_name):
        """You can guess"""

        curr = DBConnetion.get_cursor()
        connection = DBConnetion.get_connection()

        sql = (
            "UPDATE {} "
            "SET "
            "name = \"{}\" "
            "WHERE id = \"{}\" "
        ).format(UserController.table_name, new_name, user_id)

        curr.execute(sql)
        connection.commit()

        return {"users_affected": curr.rowcount}
