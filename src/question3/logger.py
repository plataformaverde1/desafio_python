#!/usr/bin/python
# -*- coding: utf-8 -*-

import logging
import os
import time
from logging.handlers import RotatingFileHandler

class LogManager:
    def __init__(self, log_name,
                 log_format=('%(asctime)s:%(name)s:%(filename)s:'
                             '%(lineno)d:%(levelname)s - %(message)s'),
                 folder='logs/', maxSize=20, backupCount=5):
        """
        Constructor.

        Parameters
        ----------
            log_name : string
                log file name.
            log_format : string
                Log formatter.
            folder : string
                Folder of storage log files.
            maxSize : int
                Maximum file size (in megabytes).
            backupCount : int
                Backup of the last 5 files.
        """
        # Logger
        self.logger = logging.getLogger(log_name)
        self.logger.setLevel(logging.DEBUG)

        if not self.logger.hasHandlers():

            # timestamp = time.strftime("%d-%m-%y_%H:%M:%S")
            timestamp = time.strftime("%d-%m-%y")
            folder += timestamp+"/"

            # Checks if storage folder exists.
            if not os.path.isdir(folder):
                os.makedirs(folder)

            # Creates logging handlers
            filename = folder + log_name + ".log"

            # Defines logging format
            formatter = logging.Formatter(log_format)

            # The rotating file handler limit file to 100MB and creates backups of the last 5 files.
            file_handler = RotatingFileHandler(
                filename, mode='a', maxBytes=maxSize*1024*1024,
                backupCount=backupCount, encoding=None, delay=False)
            file_handler.setFormatter(formatter)
            file_handler.setLevel(logging.DEBUG)

            # Channel handler
            channel_handler = logging.StreamHandler()
            channel_handler.setLevel(logging.DEBUG)
            channel_handler.setFormatter(formatter)

            # Add handlers to logger
            self.logger.addHandler(file_handler)
            self.logger.addHandler(channel_handler)

    def info(self, message):
        """
        Logging info messages.

        Parameters
        ----------
            message : string
                Message to print and store in log file.
        """
        self.logger.info(message)

    def error(self, message):
        """
        Logging error messages.

        Parameters
        ----------
            message : string
                Message to print and store in log file.
        """
        self.logger.error(message)

    def exception(self, message):
        """
        Logging exception messages.

        Parameters
        ----------
            message : string
                Message to print and store in log file.
        """

        self.logger.exception(message)

    def debug(self, message):
        """
        Logging debug messages.

        Parameters
        ----------
            message : string
                Message to print and store in log file.
        """
        self.logger.debug(message)

    def warning(self, message):
        """
        Logging warning messages.

        Parameters
        ----------
            message : string
                Message to print and store in log file.
        """
        self.logger.warning(message)

    def critical(self, message):
        """
        Logging critical messages.

        Parameters
        ----------
            message : string
                Message to print and store in log file.
        """
        self.logger.critical(message)
