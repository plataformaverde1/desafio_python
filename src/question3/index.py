
"""
3) Criar uma pequena API REST em Flask com os seguintes endpoints:
    a) um que recebe o nome do usuário, salvando nome e id em um banco de dados (deve ser um
        arquivo JSON);
    b) um que retorna os ids e nomes de todos os usuários salvos no banco de dados;
    c) um que altera o nome de um usuário já cadastrado;
    d) um que remove um usuário do banco de dados
"""

from flask import Flask, request
from flask_cors import CORS, cross_origin

from .UserController import UserController



APP = Flask(__name__)
CORS = CORS(APP)
APP.config['CORS_HEADERS'] = 'Content-Type'


# @APP.route("/test", methods=["GET"])
# @cross_origin()
# def test():
#     """ endpoint to test """
#     return {'status': "200"}


@APP.route("/user", methods=["GET"])
@cross_origin()
def get_users():
    """
    All Get users
    """
    response = UserController.index()

    return response, 200

@APP.route("/user/<user_id>", methods=["GET"])
@cross_origin()
def get_user(user_id):
    """
    Get one user
    @Parameters:
        - if specify user_id params, return info of that user, otherwise return all users
    """
    response = UserController.index(user_id=user_id)

    return response, 200

@APP.route("/user", methods=["POST"])
@cross_origin()
def create_user():
    """
    Creates a new user
    @Parameters:
        - body: {
            id: optional int
            name: string
        }

    @returns:
        - id of new user
    """
    # get body
    body = request.get_json(force=True)

    # check if request body has required keys
    try:
        body['user_name']
    except KeyError as identifier:
        return {"error": "key error: {}".format(identifier)}, 400

    response = UserController.store(user_name=body['user_name'])

    return response, 201

@APP.route("/user", methods=["PUT"])
@cross_origin()
def update_user():
    """
    updates a new user
    @Parameters:
        - body: {
            id: user_id to update
            name: new name
        }
    """
    # get body
    body = request.get_json(force=True)

    # check if request body has required keys
    try:
        body['new_name'] # pylint: disable=pointless-statement
        body['user_id'] # pylint: disable=pointless-statement
    except KeyError as identifier:
        return {"error": "key error: {}".format(identifier)}, 400

    response = UserController.update(user_id=body['user_id'], new_name=body['new_name'])

    return response, 200

@APP.route("/user", methods=["DELETE"])
@cross_origin()
def delete_user():
    """
    @Parameters:
        - body {
            user_id
        }
    """
    # get body
    body = request.get_json(force=True)

    # check if request body has required keys
    try:
        body['user_id']
    except KeyError as identifier:
        return {"error": "key error: {}".format(identifier)}, 400

    response = UserController.delete(user_id=body["user_id"])

    return response, 200


def main():
    # run app in debug mode on port 5000
    APP.run(host='0.0.0.0', debug=True, port=5000)
