# Sobre

Este repositório é referente ao teste python da empresa plataforma verde.

É composta por 3 questões. (ver [PDF](Teste_Python.pdf))


# Como executar

## Instalando dependências

- A terceira questão pede para criar uma pequena API, logo precisamos instalar o flask e o cors

```sh
pip3 install flask flask_cors
```
- Se preferir, você pode salvar em um ambiente virtual

```sh
# criar ambiente
python3 -m venv <nome_do_ambiente>

# ativar ambiente
source ./<nome_do_ambiente>/bin/activate

# e então adicionar as dependências
pip3 install flask flask_cors


# para sair do ambiente virtual digite
deactivate
```

## Executando o código

- você pode chamar os códigos individualmente
```sh
python3 index.py -q <numero_da_questao>
```


<!-- # Duvidas

- Na primeira questão pede “média de gols”. É a média do saldo de gols ou a média dos gols feitos?
    - estou usando média dos gols feitos.
- Ainda na questão 1, foi pedido que o método de atualizar o time fosse dados os seguintes atributos: resultado de uma partida, gols feitos e gols sofridos. Pode acontecer de os gols sofridos ser maior do que os gols feitos e mesmo assim considerar um empate por exemplo?
    - Não fiz esse tratamento, pois a questão explicitamente pede por esses atributos.
    - Além disso podemos inserir casos “extraordinários" também:
        - Time_A fez 2 gols, o Time_B fez 1, mas o time A foi desclassificado por qualquer motivo.
        - Time_A fez 2 gols, o Time_B, mas o jogo precisa ser adiado, ou interrompido
    - Mas posso tratar sem problema. É uma dúvida que tive.
- Na questão 2, o sql dado diz que PEDRO comprou um "jogo de pratos" e um “celular”, mas na letra a, diz que Maria que fez essas compras.
    - Acredito que os ids estejam trocados. -->



# O que poderia ser melhor

- Tratar ações com banco de dados com query builder ou ORM para abstrair o banco de dados

    - Hoje optei por usar sqlite apenas por motivo de manter o projeto simples, mas fica como possibilidade usar um um ORM, ou ao menos um query builder para, caso preciso, trocar o banco de dados (de sqlite para mysql ou postgress, por exemplo) sem precisar se preocupar com o código

- Busca de usuários com paginação

    - retornar TUDO de uma tabela como a de usuários não é uma boa opção por motivos de performance ou de quantidade de dados a serem trasnferidos, fica como sugestão implementar paginação nas buscas de usuários

- seguranca/autenticacao em rotas importantes

    - Na questão 3, está sendo feita ações "perigosas" como editar/deletar usuários. Fica como sugestão adicionar uma camada de segurança como o uso de JWT por exemplo

- TDD

    - Testes unitários dispensam qualquer tipo de justificativa pra o uso :P