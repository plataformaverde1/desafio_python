""""

"""
if __name__ == "__main__":
    import argparse

    # construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-q", "--question", required=True,
                    help="Which question to use", choices=['1', '2', '3'])
    args = vars(ap.parse_args())

    if int(args["question"]) == 1:
        from src.question1.index import main

    elif int(args["question"]) == 2:
        from src.question2.index import main

    elif int(args["question"]) == 3:
        from src.question3.index import main



    main()
